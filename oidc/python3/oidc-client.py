import argparse
import base64
import hashlib
import logging
import json
import sys
import time
import uuid
import webbrowser
from json import JSONDecodeError
from pathlib import Path
from threading import Thread
from typing import Dict, Optional, Tuple
from urllib.parse import urlencode

import requests
from cryptography.fernet import Fernet
from jose import jws
from requests import Response, Request, RequestException
from requests.auth import AuthBase

logger = logging.getLogger(__name__)


class ConfigurationException(Exception):
    """ Error in configuration. """
    pass


class OpenIDProviderError(Exception):
    """Indicates errors when communicating with the OpenID Provider."""
    pass


class OpenIDClientError(Exception):
    """Indicates errors on client-side"""
    pass


class TokenVerificationException(Exception):
    """Indicates errors during token verification"""
    pass


class ProviderRegistration:
    """Handles communication with the OpenID Provider.

    :param provider_url: url of the OpenID Provider (see :term:`Auth URL`)
    :param client_id: client id to use for provider communication (see :term:`Client ID`)
    :param client_secret: client secret for confidential clients
    """

    def __init__(self, provider_url: str, client_id: str, client_secret: str = None, **kwargs):
        self.provider_url = provider_url
        self.client_id = client_id
        self.client_secret = client_secret
        self.issuer = kwargs.get('issuer')  # type: Optional[str]
        self.grace_period = kwargs.get('grace_period', 60)
        self.audience = kwargs.get('audience')
        self.not_before = 0
        self.metadata = {}  # type: Dict[str, str]
        self.keys = {}  # type: Dict[str, str]
        if self.provider_url:
            self.provider_url = provider_url.rstrip('/')
            self.discover_metadata()

    def update_config(self, **kwargs):
        """Update the configuration parameters."""
        url_has_changed = self.provider_url != kwargs.get('provider_url')
        self.provider_url = kwargs.get('provider_url', self.provider_url)
        self.client_id = kwargs.get('client_id', self.client_id)
        self.client_secret = kwargs.get('client_secret', self.client_secret)
        self.issuer = kwargs.get('issuer', self.issuer)
        self.audience = kwargs.get('audience', self.audience)
        self.not_before = kwargs.get('not_before', self.not_before)
        self.grace_period = kwargs.get('grace_period', self.grace_period)
        self._check_cfg()
        self.provider_url = self.provider_url.rstrip('/')
        if url_has_changed:
            self.discover_metadata()

    def _check_cfg(self):
        if not self.provider_url or self.provider_url == "":
            raise ConfigurationException("Missing required config parameter: 'provider_url'")
        if not self.client_id or self.client_id == "":
            raise ConfigurationException("Missing required config parameter: 'client_id")

    def discover_metadata(self, timeout=300):
        """Uses OpenID Connect Discovery to acquire information about the OpenID Provider, e.g. endpoint urls.

        :param timeout: timeout in seconds to wait for Provider before giving up
        :raises RuntimeError: If the Provider cannot be reached in the given time
        """
        timeout = time.time() + timeout
        suppress_errors = False
        while time.time() < timeout:
            try:
                r = requests.get("{}/.well-known/openid-configuration".format(self.provider_url))
                if r.status_code == 200:
                    self.metadata = r.json()
                    if not self.issuer:
                        self.issuer = self.metadata['issuer']
                    if 'jwks_uri' in self.metadata:
                        jwks = requests.get(self.metadata['jwks_uri'])
                        parsed_jwks = jwks.json()["keys"]
                        for key in parsed_jwks:
                            self.keys[key["kid"]] = key
                    if suppress_errors:
                        logger.info("Eventually succeeded in retrieving IdP config")
                    return
                elif not suppress_errors:
                    logger.warning("Got %i response when requesting IdP config", r.status_code)
                    suppress_errors = True
            except RequestException as e:
                if not suppress_errors:
                    logger.warning("Unable to retrieve config from IdP: %s", str(e))
                    suppress_errors = True
            except JSONDecodeError as e:
                if not suppress_errors:
                    logger.warning("Unable to parse response from IdP: %s", str(e))
                    suppress_errors = True
            except Exception as e:
                logger.error("Uncaught exception occurred while requesting IdP config: %s", str(e))
            logger.info("Waiting for IdP...")
            time.sleep(10)
        raise RuntimeError("Hit timeout while waiting for IdP!")

    @property
    def token_endpoint(self) -> str:
        """Get the Keycloak Token Endpoint url.

        :return: url of the Keycloak token endpoint
        :rtype: str
        """
        return self.metadata['token_endpoint']

    def verify_id_token(self, id_token: str, nonce: Optional[str] = None) -> Dict[str, str]:
        """Validate signature of an identity token and return the token claims.
        Note, that Open ID requires id token to be an JWT.

        :param id_token: the jwt encoded token
        :param nonce: a nonce that was used in the code flow (optional)
        :return: the decoded and validated token claims
        :raise TokenVerificationException: if the token is not valid / cannot be validated
        """
        if not id_token or id_token == "":
            raise TokenVerificationException("No identity token specified")

        try:
            header = jws.get_unverified_header(id_token)
        except Exception:
            raise TokenVerificationException("Could not decode header of id token")

        if header.get('kid') in self.keys and header.get('alg') in jws.ALGORITHMS.SUPPORTED:
            # we can locally check validity
            try:
                verified_token = jws.verify(id_token, self.keys[header['kid']], algorithms=header['alg'])
                parsed_token = json.loads(verified_token)
                if nonce and ('nonce' not in parsed_token or parsed_token['nonce'] != nonce):
                    raise TokenVerificationException("Nonce mismatch!")
                return parsed_token
            except Exception:
                raise TokenVerificationException("Invalid identity token")
        else:
            raise TokenVerificationException("Identity token signed with unknown key or algorithm!")

class OidcClientAuth(AuthBase):
    """Handles authentication with the OIDC Provider for client applications.
    Supports the Authentication Code Flow, the Resource Owner Password Grant and the Client Credentials Grant.

    The adapter can be directly passed to the ``requests`` module and automatically adds the authorization header.
    If the access token has expired, it will be refreshed before the request is executed::

        auth = OidcClientAuth(provider_url, client_id)
        auth.authenticate(username, password)
        r = requests.get(another_protected_url, auth=auth)

    :param provider_url: url of the OpenID Provider to use (see :term:`Auth URL`)
    :param client_id: client id to use for communication with provider (see :term:`Client ID`)
    :param client_secret: client secret to use for communication with provider (optional, required for service accounts)
    :param background_refresh: maintain a valid refresh token even if no requests are made regularly (defaults to False)

    """
    def __init__(self, provider_url: str, client_id: str, client_secret: str = None, background_refresh=False):
        self.provider = ProviderRegistration(provider_url, client_id, client_secret)
        self.access_token = None  # type: Optional[str]
        self.refresh_token = None  # type: Optional[str]
        self.identity = None  # type: Optional[Dict[str, str]]
        self.exp = None  # type: Optional[int]
        self.refresh_exp = None  # type: Optional[int]
        self._client_credentials_grant = False
        self._client_credentials_grant_scopes = None  # type: Optional[str]
        self._read_token()
        if background_refresh:
            self._start_refresh_worker()

    def _token_request(self, data: Dict[str, str], nonce: str = None) -> Response:
        resp = requests.post(self.provider.token_endpoint, data=data)
        if resp.status_code == 200:
            data = resp.json()
            self.access_token = data['access_token']
            self.refresh_token = data['refresh_token']
            self.exp = int(time.time() + int(data['expires_in']) - 5)
            refresh_expires_in = int(data['refresh_expires_in'])
            if refresh_expires_in > 0:
                # we got a normal refresh token
                self.refresh_exp = int(time.time() + refresh_expires_in - 5)
            else:
                # we got an offline token with long lifetime
                self.refresh_exp = None

            # only present if 'openid' scope was requested
            if 'id_token' in data:
                self.identity = self.provider.verify_id_token(data['id_token'], nonce)
        return resp

    def _start_refresh_worker(self):
        def refresh_regularly():
            while True:
                try:
                    if self.refresh_exp:
                        sleep_time = self.refresh_exp - time.time()
                        if sleep_time > 0:
                            time.sleep(sleep_time)
                        self.maintain_token()
                    else:
                        if self.refresh_token:
                            # we have an offline token that should only be renewed infrequently
                            time.sleep(86400 * 7)  # one week
                        else:
                            # no tokens available yet. Retry in 30 seconds
                            time.sleep(30)
                except Exception as e:
                    logger.warning("Error occurred in background refresh worker: " + str(e))

        refresh_thread = Thread(target=refresh_regularly, daemon=True)
        refresh_thread.start()

    def _get_token_file_params(self) -> Tuple[Path, bytes]:
        # store encrypted token under hashed filename
        filename = ".".join(["refresh_token", self.provider.issuer or "", self.provider.client_id])
        filename_hash = hashlib.sha1(filename.encode()).hexdigest()
        key = base64.b64encode(hashlib.sha256(filename.encode()).digest())
        path = Path.home() / ".aedifion" / filename_hash
        return path, key

    def store_token(self):
        """Stores the current refresh token on disk.

        .. Note::

           Uses the token type, issuer and client_id as symmetric encryption key.
           Thus, the refresh token can be decrypted if this information is known!
        """
        if self.refresh_token:
            path, key = self._get_token_file_params()
            path.parents[0].mkdir(exist_ok=True)
            f = Fernet(key)
            with path.open(mode='wb') as file:
                file.write(f.encrypt(self.refresh_token.encode('UTF-8')))

    def _read_token(self):
        path, key = self._get_token_file_params()
        if path.exists():
            f = Fernet(key)
            with path.open(mode='rb') as file:
                token = str(f.decrypt(file.read()), encoding='UTF-8')
                try:
                    unverified_claims = json.loads(jws.get_unverified_claims(token))
                    issuer = unverified_claims['iss']
                    if issuer == self.provider.issuer:
                        self.refresh_token = token
                        self.maintain_token()
                    else:
                        logger.warning("Found refresh token with mismatching issuer!")
                except Exception as e:
                    logger.warning("Unable to load stored token: " + str(e))

    def authenticate(self, username: str, password: str, offline_access=False):
        """Use the Resource Owner Password Grant to retrieve an access token.

        :param username: the username of the user to retrieve the token for
        :param password: the password of the user to retrieve the token for
        :param offline_access: request a long-lived refresh token for non-interactive clients (optional)

        .. Note::

            When using with ``offline_access`` the token is saved in your home directoy
            and automatically loaded on initialization.
        """
        data = {
            'username': username,
            'password': password,
            'grant_type': 'password',
            'client_id': self.provider.client_id,
            'scope': 'openid'
        }
        if self.provider.client_secret:
            data['client_secret'] = self.provider.client_secret
        if offline_access:
            data['scope'] += " offline_access"

        resp = self._token_request(data)
        if not resp.status_code == 200:
            raise OpenIDClientError("Unable to authenticate: '{}'".format(resp.text))
        self._client_credentials_grant = False

        if offline_access:
            self.store_token()

    def enable_service_account(self, scopes: str = None):
        """Performs the Client Credentials Grant, i.e., request a token using the ``client_id`` and ``client_secret``.
        This requires the ``Service Account`` option to be enabled in Keycloak.
        :param scopes: an optional list of scopes to request (separated by whitespace)
        """
        assert self.provider.client_secret is not None
        data = {
            'grant_type': 'client_credentials',
            'client_id': self.provider.client_id,
            'client_secret': self.provider.client_secret
        }
        self._client_credentials_grant_scopes = scopes or self._client_credentials_grant_scopes
        if self._client_credentials_grant_scopes:
            data['scope'] = self._client_credentials_grant_scopes
        logger.debug("Performing ClientCredentials Grant")
        resp = self._token_request(data)
        if not resp.status_code == 200:
            logger.warning("Unable to perform the client credentials grant: {}".format(resp.text))
            raise OpenIDProviderError(resp.text)
        self._client_credentials_grant = True

    def get_auth_url(self, scopes: str = None, redirect_uri=None, offline_access=False) -> Tuple[str, str]:
        """Returns a url for initiating the Authentication Code Flow (a.k.a. Basic Flow).

        When accessing the url, the user is asked for his credentials (if not yet authenticated in the current session).
        After a successful login the authorization code is sent to the ``redirect_uri`` or displayed in the browser.

        :param scopes: list of additional scopes to be requested (optional)
        :param redirect_uri: uri to send the code to (optional, has to be registered with the Provider out of band)
        :param offline_access: request a long lived refresh token for non-interactive access (optional)

        :returns: the url that initiates the auth code flow and a nonce that is handed to :meth:`~exchange_code>`
        :rtype: Tuple[str, str]
        """
        nonce = str(uuid.uuid4())
        scope_list = scopes.split() if scopes else []
        scope_list.append('openid')
        if offline_access:
            scope_list.append('offline_access')
        data = {
            'response_type': 'code',
            'scope': " ".join(scope_list),
            'client_id': self.provider.client_id,
            'redirect_uri': redirect_uri or 'urn:ietf:wg:oauth:2.0:oob',
            'nonce': nonce
        }
        url = self.provider.metadata['authorization_endpoint'] + '?' + urlencode(data)
        return url, nonce

    def exchange_code(self, authorization_code: str, nonce: str, redirect_uri: str = None):
        """Exchange a code obtained via the Authorization Code Flow for access and refresh tokens.
        See also :meth:`~aedifion_core.auth.get_auth_url`.

        :param authorization_code: the code retrieved using the Auth Code Flow
        :param nonce: nonce that was retrieved from :meth:`~get_auth_url`
        :param redirect_uri: the redirect uri that the code was sent to
        """
        data = {
            'grant_type': 'authorization_code',
            'client_id': self.provider.client_id,
            'code': authorization_code,
            'redirect_uri': redirect_uri or 'urn:ietf:wg:oauth:2.0:oob'
        }
        if self.provider.client_secret:
            data['client_secret'] = self.provider.client_secret
        resp = self._token_request(data, nonce)
        if resp.status_code != 200:
            logger.warning("Unable to exchange obtained code for tokens: '{}'".format(resp.text))
        self._client_credentials_grant = False

    def maintain_token(self):
        """Renews the access token if expired."""

        if self.exp and time.time() > self.exp or self.refresh_token and not self.access_token:
            if self.refresh_exp and time.time() > self.refresh_exp:
                if self._client_credentials_grant:
                    self.enable_service_account()
                    return
                else:
                    raise OpenIDClientError("Refresh token has expired since {:.01f} sec".format(
                        time.time() - self.refresh_exp))

            # token expired, try to refresh
            logger.debug("Refreshing access token")
            data = {
                'client_id': self.provider.client_id,
                'grant_type': 'refresh_token',
                'refresh_token': self.refresh_token
            }
            if self.provider.client_secret:
                data['client_secret'] = self.provider.client_secret

            resp = self._token_request(data)
            if not resp.status_code == 200:
                if self._client_credentials_grant:
                    self.enable_service_account()
                    return
                raise OpenIDClientError("Unable to refresh token: '{}'".format(resp.text))

    def __call__(self, req: Request) -> Request:  # type: ignore
        self.maintain_token()
        if self.access_token:
            req.headers['Authorization'] = "Bearer " + self.access_token
        return req


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='aedifion IdM Login Utility')
    parser.add_argument('-u', '--url',
                        type=str,
                        help="URL of the authentication provider",
                        default="https://auth.aedifion.io/auth/realms/aedifion")
    parser.add_argument('-c', '--client',
                        type=str,
                        help="name of the client to authenticate", required=True)
    parser.add_argument('-p', '--use-password',
                        action='store_true',
                        help="Enter the password on the console")
    parser.add_argument('--api',
                        type=str,
                        help='URL to http api',
                        default='https://api.aedifion.io')
    args = parser.parse_args()
    print("Logging in to the aedifion Identity Management")
    print(" - Server: {}".format(args.url))
    print(" - Client: {}".format(args.client))

    auth = OidcClientAuth(args.url, args.client)
    if args.use_password:
        username = input("Username: ")
        password = input("Password: ")
        try:
            auth.authenticate(username, password, offline_access=True)
        except OpenIDClientError as e:
            print("Something went wrong during authentication: " + str(e))
            sys.exit(1)
    else:
        if auth.access_token:
            result = input("You are already logged in as {}. Proceed? [y/N]: ".format(
                auth.identity['name'] if auth.identity else 'N/A'))
            if result.lower() != 'y':
                sys.exit(0)
        else:
            url, nonce = auth.get_auth_url(offline_access=True)
            try:
                webbrowser.open(url)
            except Exception:
                print("Please, use a browser to open the following url:")
                print(url)
            code = input("Please, paste the displayed authorization code: ")
            try:
                auth.exchange_code(code, nonce)
                auth.store_token()
            except OpenIDClientError as e:
                print("Something went wrong during authentication: " + str(e))
                sys.exit(1)

    print("Hello {}! You have been successfully authenticated.".format(
        auth.identity['given_name'] if auth.identity else "N/A"))

    # Make an actual request to the API
    r = requests.get("{}/v2/user".format(args.api), auth=auth)
    print(r.text)

